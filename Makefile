# $Id: Makefile,v 1.5 2005/05/02 13:58:45 spa Exp $

# Makefile for gprolog timers

TARGET=gprolog-timer

PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
LIBDIR=$(PREFIX)/lib/isco

GPLC=gplc

LIBS=-lrt
CFLAGS=-g

OBJECTS=timer_c.o
PLFILES=timer.pl

all: $(TARGET) timer-full.o
	touch .timestamp

$(TARGET): $(PLFILES) $(OBJECTS)
	$(GPLC) -o $(TARGET) $^ -L $(LIBS)

install: $(TARGET)
	install -c -m 555 $(TARGET) $(BINDIR)
	install -c -m 444 $(OBJECTS) $(PLFILES) $(LIBDIR)

clean::
	rm -f $(TARGET) *.o *~ \#* .timestamp

timer-full.o: timer_c.o timer.o
	ld -r -o $@ timer_c.o timer.o -L/usr/lib -L/usr/lib/i386-linux-gnu -lrt

%.o:: %.c
	$(GPLC) -C "$(CFLAGS)" -c $<

%.o:: %.pl
	$(GPLC) -C "$(CFLAGS)" -c $<
