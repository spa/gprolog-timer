/* ----------------------------------------------------------------------------
 * TIMER is Copyright (C) 2013 Salvador Abreu
 * 
 *    This program  is free software;  you can redistribute  it and/or
 *    modify  it under  the terms  of  the GNU  Lesser General  Public
 *    License  as published  by the  Free Software  Foundation; either
 *    version 2.1, or (at your option) any later version.
 * 
 *    This program is distributed in  the hope that it will be useful,
 *    but WITHOUT  ANY WARRANTY; without even the  implied warranty of
 *    MERCHANTABILITY or  FITNESS FOR  A PARTICULAR PURPOSE.   See the
 *    GNU Lesser General Public License for more details.
 * 
 *    You should have received a copy of the GNU Lesser General Public
 *    License  along with  this program;  if  not, write  to the  Free
 *    Software Foundation, Inc., 59  Temple Place - Suite 330, Boston,
 *    MA 02111-1307, USA.
 * 
 * On Debian  GNU/Linux systems, the  complete text of the  GNU Lesser
 * General Public License is found in /usr/share/common-licenses/LGPL.
 * ----------------------------------------------------------------------------
 */

/* ============================================================================
 *
 * GNU Prolog timer alarm interface
 *   
 */

#include <stdlib.h>
#include <unistd.h>

#include <signal.h>
#include <time.h>

#include <gprolog.h>


#if (__GPROLOG_VERSION__ < 10300) /* for use in older versions */

typedef enum { PL_FALSE, PL_TRUE } PlBool;
typedef long PlLong;
typedef unsigned long PlULong;

#define Pl_Create_Atom Create_Atom
#define Pl_Mk_Integer Mk_Integer
#define Pl_Rd_Integer_Check Rd_Integer_Check
#define Pl_Rd_Integer Rd_Integer

#endif


//-----------------------------------------------------------------------------

static int nhandlers = 0;
static void *handlers[1000] = { NULL, };

static inline int timer_h_insert_lookup (void *value, int insertp) {
  int i = nhandlers;
  void **p = handlers;
  void **p1 = NULL;

  while (i > 0) {
    if (*p == value)
      return p-handlers;
    else if (*p)
      --i;
    else
      p1 = p;
    ++p;
  }

  if (!insertp)
    return -1;

  ++nhandlers;
  if (!p1) {
    p1 = p;
  }
  *p1 = value;

  return p1-handlers;
}

static inline int timer_h_insert (void *value) {
  return timer_h_insert_lookup (value, TRUE);
}

static inline int timer_h_lookup (void *value) {
  return timer_h_insert_lookup (value, FALSE);
}


//-----------------------------------------------------------------------------



/*-------------------------------------------------------------------------*
 * SIGALRM_HANDLER                                                      *
 *                                                                         *
 * Handler for timer alarms.                                               *
 *-------------------------------------------------------------------------*/

static void
SIGALRM_Handler (int sig, siginfo_t *sip, void *arg)
{
  // TODO: improve when Pl_Throw() becomes available
  Pl_Err_System (Pl_Create_Atom ("time_limit_exceeded"));
}


/*-------------------------------------------------------------------------*
 * PL_SETUP_SIGALRM_HANDLER                                                *
 *                                                                         *
 * Make sure the handler for timer alarms is enabled and not blocked.      *
 *-------------------------------------------------------------------------*/

void Install_SIGALRM_Handler ()
{
  struct sigaction act, oldact;
  sigset_t set, oldset;

  act.sa_sigaction = (void (*) ()) &SIGALRM_Handler;
  sigemptyset (&act.sa_mask);
  act.sa_flags = SA_SIGINFO;

  sigemptyset (&set);
  sigaddset (&set, SIGALRM);

  sigprocmask (SIG_UNBLOCK, &set, &oldset);
  sigaction (SIGALRM, &act, &oldact);
}


/*-------------------------------------------------------------------------*
 * PL_TIMER_CREATE                                                         *
 *                                                                         *
 * Creates a timer structure and returns a handle for it.                  *
 *-------------------------------------------------------------------------*/

PlBool pl_timer_create (PlLong id, PlLong event, PlLong *iid)
{
  struct sigevent sev;
  timer_t timerid;

  sev.sigev_notify = event;	/* may be SIGNAL or NONE */
  sev.sigev_signo = SIGALRM;	/* in case of SIGNAL -> ALRM only */
  sev.sigev_value.sival_int = 0; /* ignored */

  if (!timer_create ((clockid_t) id, &sev, &timerid)) {
    *iid = timer_h_insert ((void *) timerid);
    return TRUE;
  }
  return FALSE;
}


/*-------------------------------------------------------------------------*
 * PL_TIMER_DELETE                                                         *
 *                                                                         *
 * Deletes a timer structure.                                              *
 *-------------------------------------------------------------------------*/

PlBool pl_timer_delete (PlLong iid)
{
  timer_t id;

  // TODO: check range of iid
  id = handlers[iid];
  if (id) {
    handlers[iid] = 0;
    --nhandlers;

    return ! timer_delete (id);
  }
  return TRUE;
}


/*-------------------------------------------------------------------------*
 * PL_TIMER_GET                                                            *
 *                                                                         *
 * Get information on a timer structure.                                   *
 *-------------------------------------------------------------------------*/

PlBool pl_timer_get (PlLong iid, PlULong *it_interval, PlULong *it_value)
{
  struct itimerspec curr_value;
  PlBool ret = ! timer_gettime ((timer_t) handlers[iid], &curr_value);

  if (ret) {			// Express times in milliseconds
    *it_interval =
      curr_value.it_interval.tv_sec * 1000 +
      curr_value.it_interval.tv_nsec / 1000 / 1000;
    *it_value =
      curr_value.it_value.tv_sec * 1000 +
      curr_value.it_value.tv_nsec / 1000 / 1000;
  }

  return ret;
}


/*-------------------------------------------------------------------------*
 * PL_TIMER_SET                                                            *
 *                                                                         *
 * Get a timer going.                                                      *
 *-------------------------------------------------------------------------*/

PlBool pl_timer_set (PlLong iid, PlLong flags,
		     PlULong nit_interval,
		     PlULong nit_value)
{
  struct itimerspec new_value, old_value;

  new_value.it_interval.tv_sec = nit_interval / 1000;
  new_value.it_interval.tv_nsec = (nit_interval % 1000) * 1000 * 1000;
  new_value.it_value.tv_sec = nit_value / 1000;
  new_value.it_value.tv_nsec = (nit_value % 1000) * 1000 * 1000;

  Install_SIGALRM_Handler ();

  return ! timer_settime ((timer_t) handlers[iid], flags,
			  &new_value, &old_value);
}


