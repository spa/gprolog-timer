/* ----------------------------------------------------------------------------
 * TIMER is Copyright (C) 2013 Salvador Abreu
 * 
 *    This program  is free software;  you can redistribute  it and/or
 *    modify  it under  the terms  of  the GNU  Lesser General  Public
 *    License  as published  by the  Free Software  Foundation; either
 *    version 2.1, or (at your option) any later version.
 * 
 *    This program is distributed in  the hope that it will be useful,
 *    but WITHOUT  ANY WARRANTY; without even the  implied warranty of
 *    MERCHANTABILITY or  FITNESS FOR  A PARTICULAR PURPOSE.   See the
 *    GNU Lesser General Public License for more details.
 * 
 *    You should have received a copy of the GNU Lesser General Public
 *    License  along with  this program;  if  not, write  to the  Free
 *    Software Foundation, Inc., 59  Temple Place - Suite 330, Boston,
 *    MA 02111-1307, USA.
 * 
 * On Debian  GNU/Linux systems, the  complete text of the  GNU Lesser
 * General Public License is found in /usr/share/common-licenses/LGPL.
 * ----------------------------------------------------------------------------
 */

%% :- module(timer, [timer_create/3,
%% 		  timer_delete/1,
%% 		  timer_get/2,
%% 		  timer_set/4,
%% 		  call_with_time_limit/2]).

:- dynamic('$timers_timer'/1).


% -- from /usr/include/time.h -------------------------------------------------

timer_constant(realtime,           0).
timer_constant(monotonic,          1).
timer_constant(process_cputime_id, 2).
timer_constant(thread_cputime_id,  3).
timer_constant(monotonic_raw,      4).
timer_constant(realtime_coarse,    5).
timer_constant(monotonic_coarse,   6).
timer_constant(boottime,           7).
timer_constant(realtime_alarm,     8).
timer_constant(boottime_alarm,     9).

% -- from /usr/include/*/siginfo.h --------------------------------------------

sigev_constant(signal, 0).	% we always mean SIGALRM here
sigev_constant(none, 1).	% don't notify

% -----------------------------------------------------------------------------

:- foreign(pl_timer_create(+integer, +integer, -integer)).
:- foreign(pl_timer_delete(+integer)).
:- foreign(pl_timer_get(+integer, -integer, -integer)).
:- foreign(pl_timer_set(+integer, +integer, +integer, +integer)).

% timers are expressed in *milliseconds*; we deliberately hold off on
% the full range and precision, in order to make things easier and
% more conventional for Prolog.

% -----------------------------------------------------------------------------

timer_create(ID, EVENT, TIMERID) :-
    timer_constant(ID, IID),
    sigev_constant(EVENT, IEVENT),
    pl_timer_create(IID, IEVENT, TIMERID).

timer_delete(TIMERID) :-
    pl_timer_delete(TIMERID).

timer_get(TIMERID, INTERVAL, VALUE) :-
    pl_timer_get(TIMERID, INTERVAL, VALUE).

timer_set(TIMERID, FLAGS, NINT, NVAL) :-
    pl_timer_set(TIMERID, FLAGS, NINT, NVAL).

timer_set(TIMERID, FLAGS, OINT, NINT, OVAL, NVAL) :-
    pl_timer_get(TIMERID, OINT, OVAL),
    pl_timer_set(TIMERID, FLAGS, NINT, NVAL).

% -----------------------------------------------------------------------------

call_with_time_limit(TIME, GOAL) :-
    kill_timers,
    timer_create(realtime, signal, TIMERID),
    assertz('$timers_timer'(TIMERID)),
    timer_set(TIMERID, 0, 0, TIME),
    call(GOAL), !,
    timer_delete(TIMERID).


kill_timers :- retract('$timers_timer'(TIMERID)), timer_delete(TIMERID), fail.
kill_timers.

% -----------------------------------------------------------------------------

% Local Variables:
% mode: Prolog
% End:
